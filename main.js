/**
 * テストのすべての教科の合計点。
 * @param {number} 国語
 * @param {number} 数学
 * @param {number} 英語
 * @returns {number}
 */
function 合計(国語, 数学, 英語) {
  return 国語 + 数学 + 英語;
}

/**
 * テストのすべての教科の平均点。
 * @param {number} 国語
 * @param {number} 数学
 * @param {number} 英語
 * @returns {number}
 */
function 平均(国語, 数学, 英語) {
  return 合計(国語, 数学, 英語) / 3;
}

/**
 * テストのすべての教科の成績。
 * @param {number} 国語
 * @param {number} 数学
 * @param {number} 英語
 * @returns {string}
 */
function 成績(国語, 数学, 英語) {
  return `
国語: ${国語}
数学: ${数学}
英語: ${英語}
合計: ${合計(国語, 数学, 英語)}
平均: ${平均(国語, 数学, 英語)}
`;
}

console.log(成績(87, 70, 68));
